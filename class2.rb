
class Taiyaki #たいやきクラス(鯛焼きの型)を作っていく。
end

__END__

class Taiyaki
  attr_reader :anko #anko変数の読みを可能にする。

  def initialize(anko) #初期化メソッド
    @anko = anko #インスタンス変数を定義
  end
end

taiyaki1 = Taiyaki.new("つぶあん")
taiyaki2 = Taiyaki.new("こしあん")

puts taiyaki1.anko
puts taiyaki2.anko



class Taiyaki
  attr_reader :anko, :size

  def initialize(anko, size) #initializeメソッド
    @anko = anko #インスタンス変数
    @size = size
  end

  def show() #インスタンスメソッド
    puts "あんこ: #{anko}, 大きさ: #{size}cm"
  end
end

taiyaki3 = Taiyaki.new("チーズ", 11)
taiyaki3.show
